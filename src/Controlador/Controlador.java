package Controlador;
/**
 *
 * @author Manuel
 */
import Modelo.Docente;
import vista.dlgvista;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {

    private Docente docente;
    private dlgvista vista;

    public Controlador(Docente docente, dlgvista vista) {
        this.docente = docente;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

        vista.boxNivel.addActionListener(this);
        vista.boxHijos.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle(":: Control de Pagos a Docentes ::");
        vista.setSize(420, 520);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == vista.btnNuevo) {
                vista.btnGuardar.setEnabled(true);
                vista.txtNumDocente.setEnabled(true);
                vista.txtNombre.setEnabled(true);
                vista.txtDomicilio.setEnabled(true);
                vista.txtPagoBase.setEnabled(true);
                vista.txtHorasImpartidas.setEnabled(true);
                vista.boxNivel.setEnabled(true);
                vista.boxHijos.setEnabled(true);

                limpiarCampos();
            } else if (e.getSource() == vista.btnGuardar) {
                vista.btnMostrar.setEnabled(true);
                int opcion = JOptionPane.showConfirmDialog(vista, "¿Desea guardar los datos?", "Guardar", JOptionPane.YES_NO_OPTION);
                if (opcion == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(vista, "Se han guardado los datos.");
                }
            } else if (e.getSource() == vista.btnMostrar) {
                double pagoBase = Double.parseDouble(vista.txtPagoBase.getText());
                int horasImpartidas = Integer.parseInt(vista.txtHorasImpartidas.getText());
                String nivel = vista.boxNivel.getSelectedItem().toString();
                String hijos = vista.boxHijos.getSelectedItem().toString();

                double pagoHoras = horasImpartidas * 130;
                double pagoBono = nivel.equals("A") ? 130 : 130;
                double descuentoImpuestos = hijos.equals("Sí") ? (pagoBase * 0.16) : 416;
                double totalPagar = pagoBase + pagoHoras + pagoBono - descuentoImpuestos;

                vista.txtPagoHoras.setText(String.valueOf(pagoHoras));
                vista.txtPagoBono.setText(String.valueOf(pagoBono));
                vista.txtDescuentoImpuestos.setText(String.valueOf(descuentoImpuestos));
                vista.txtTotalPagar.setText(String.valueOf(totalPagar));

                JOptionPane.showMessageDialog(vista, "Cálculos realizados correctamente");
            } else if (e.getSource() == vista.btnLimpiar) {
                limpiarCampos();
            } else if (e.getSource() == vista.btnCancelar) {
                deshabilitarCampos();
                deshabilitarBotonGuardar();
            } else if (e.getSource() == vista.btnCerrar) {
                cerrarPrograma();
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(vista, "Error: Ingrese valores numéricos válidos en los campos requeridos.", "Error de entrada", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void habilitarCampos() {
        vista.txtNumDocente.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtDomicilio.setEnabled(true);
        vista.txtPagoBase.setEnabled(true);
        vista.txtHorasImpartidas.setEnabled(true);
        vista.boxNivel.setEnabled(true);
        vista.boxHijos.setEnabled(true);
    }

    private void deshabilitarCampos() {
        vista.txtNumDocente.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtDomicilio.setEnabled(false);
        vista.txtPagoBase.setEnabled(false);
        vista.txtHorasImpartidas.setEnabled(false);
        vista.boxNivel.setEnabled(false);
        vista.boxHijos.setEnabled(false);
    }

    private void habilitarBotonGuardar() {
        vista.btnGuardar.setEnabled(true);
    }

    private void deshabilitarBotonGuardar() {
        vista.btnGuardar.setEnabled(false);
    }

    private void limpiarCampos() {
        vista.txtNumDocente.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.txtPagoBase.setText("");
        vista.txtHorasImpartidas.setText("");
        vista.boxNivel.setSelectedIndex(0);
        vista.boxHijos.setSelectedIndex(0);
        vista.txtPagoHoras.setText("");
        vista.txtPagoBono.setText("");
        vista.txtDescuentoImpuestos.setText("");
        vista.txtTotalPagar.setText("");
    }

    private void cerrarPrograma() {
        System.exit(0);
    }

    public static void main(String[] args) {
        Docente docente = new Docente();
        dlgvista vista = new dlgvista();
        Controlador controlador = new Controlador(docente, vista);
        controlador.iniciarVista();
    }
}

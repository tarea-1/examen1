package Modelo;
/**
 *
 * @author Manuel
 */
public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private double pagoBase;
    private int horas;

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public double getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(double pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public double calcularPago() {
        double incrementoPorcentaje = 0.0;
        if (nivel == 1) {
            incrementoPorcentaje = 0.3;
        } else if (nivel == 2) {
            incrementoPorcentaje = 0.5;
        } else if (nivel == 3) {
            incrementoPorcentaje = 1.0;
        }
        double pagoBaseIncrementado = pagoBase * (1 + incrementoPorcentaje);
        return pagoBaseIncrementado * horas;
    }

    public double calcularImpuesto() {
        double pagoTotal = calcularPago();
        return pagoTotal * 0.16;
    }

    public double calcularBono() {
        double bono = 0.0;
        int cantidadHijos = 0; 

        if (cantidadHijos >= 1 && cantidadHijos <= 2) {
            bono = calcularPago() * 0.05;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            bono = calcularPago() * 0.1;
        } else if (cantidadHijos > 5) {
            bono = calcularPago() * 0.2;
        }

        return bono;
    }
}
